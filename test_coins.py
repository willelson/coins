import unittest
from coins import combinations_for_coin, get_all_combinations


class TestCoins(unittest.TestCase):

    def test_combinations_for_coin_1(self):
        target = 4
        coin = 2

        expected_combos_for_coin = [
            [2, 2],
            [2, 1, 1]
        ]

        combos_for_coin = combinations_for_coin(target, coin)

        self.assertEqual(len(combos_for_coin),
                         len(expected_combos_for_coin))

        for combo in expected_combos_for_coin:
            self.assertTrue(combo[0] == coin)
            self.assertIn(combo, combos_for_coin)

    def test_combinations_for_coin_2(self):
        target = 9
        coin = 2

        expected_combos_for_coin = [
            [2, 2, 2, 2, 1],
            [2, 2, 2, 1, 1, 1],
            [2, 2, 1, 1, 1, 1, 1],
            [2, 1, 1, 1, 1, 1, 1, 1]
        ]

        combos_for_coin = combinations_for_coin(target, coin)

        self.assertEqual(len(combos_for_coin),
                         len(expected_combos_for_coin))

        for combo in expected_combos_for_coin:
            self.assertTrue(combo[0] == coin)
            self.assertIn(combo, combos_for_coin)

    def test_combinations_for_coin_3(self):
        target = 9
        coin = 2

        expected_combos_for_coin = [
            [2, 2, 2, 2, 1],
            [2, 2, 2, 1, 1, 1],
            [2, 2, 1, 1, 1, 1, 1],
            [2, 1, 1, 1, 1, 1, 1, 1]
        ]

        combos_for_coin = combinations_for_coin(target, coin)

        self.assertEqual(len(combos_for_coin),
                         len(expected_combos_for_coin))

        for combo in expected_combos_for_coin:
            self.assertTrue(combo[0] == coin)
            self.assertIn(combo, combos_for_coin)

    def test_combinations_for_coin_4(self):
        target = 9
        coin = 5

        expected_combos_for_coin = [
            [5, 2, 2],
            [5, 2, 1, 1],
            [5, 1, 1, 1, 1]
        ]

        combos_for_coin = combinations_for_coin(target, coin)

        self.assertEqual(len(combos_for_coin),
                         len(expected_combos_for_coin))

        for combo in expected_combos_for_coin:
            self.assertTrue(combo[0] == coin)
            self.assertIn(combo, combos_for_coin)

    def test_all_combos_5(self):
        target = 5

        expected_combos = [
            [5],
            [2, 1, 1, 1],
            [2, 2, 1],
            [1, 1, 1, 1, 1]
        ]

        all_combos = get_all_combinations(target)

        self.assertEqual(len(all_combos),
                         len(expected_combos))

        for combo in expected_combos:
            self.assertIn(combo, all_combos)


if __name__ == '__main__':
    unittest.main()
